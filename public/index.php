<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }
  
  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['field-name-1'] = !empty($_COOKIE['field-name-1_error']);
  $errors['field-email'] = !empty($_COOKIE['field-email_error']);
  $errors['field-date'] = !empty($_COOKIE['field-date_error']);
  $errors['radio-group-1'] = !empty($_COOKIE['radio-group-1_error']);
  $errors['radio-group-2'] = !empty($_COOKIE['radio-group-2_error']);
  $errors['field-name-4'] = !empty($_COOKIE['field-name-4_error']);
  $errors['field-name-2'] = !empty($_COOKIE['field-name-2_error']);
  $errors['check-1'] = !empty($_COOKIE['check-1_error']);
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['field-name-1']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('field-name-1_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя корректно.</div>';
  }
  
    // Выдаем сообщения об ошибках.
  if ($errors['field-email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('field-email_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните email.</div>';
  }
  
    // Выдаем сообщения об ошибках.
  if ($errors['field-date']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('field-date_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Когда вы родились?</div>';
  }
  
    // Выдаем сообщения об ошибках.
  if ($errors['radio-group-1']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('radio-group-1_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Пол надо указать!</div>';
  }
  
    // Выдаем сообщения об ошибках.
  if ($errors['radio-group-2']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('radio-group-2_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Указать кол-во конечностей! Это важно!</div>';
  }
  
    // Выдаем сообщения об ошибках.
  if ($errors['field-name-4']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('field-name-4_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Укажи суперспособность!</div>';
  }
  
    // Выдаем сообщения об ошибках.
  if ($errors['field-name-2']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('field-name-2_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Напиши что-то!</div>';
  }
  
    // Выдаем сообщения об ошибках.
  if ($errors['check-1']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('check-1_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Поставь галочку!</div>';
  }
  // TODO: тут выдать сообщения об ошибках в других полях.

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['field-name-1'] = empty($_COOKIE['field-name-1_value']) ? '' : $_COOKIE['field-name-1_value'];
  $values['field-email'] = empty($_COOKIE['field-email_value']) ? '' : $_COOKIE['field-email_value'];
  $values['field-date'] = empty($_COOKIE['field-date_value']) ? '' : $_COOKIE['field-date_value'];
  $values['radio-group-1'] = empty($_COOKIE['radio-group-1_value']) ? '' : $_COOKIE['radio-group-1_value'];
  $values['radio-group-2'] = empty($_COOKIE['radio-group-2_value']) ? '' : $_COOKIE['radio-group-2_value'];
  $values['field-name-4'] = empty($_COOKIE['field-name-4_value']) ? '' : $_COOKIE['field-name-4_value'];
  $values['field-name-2'] = empty($_COOKIE['field-name-2_value']) ? '' : $_COOKIE['field-name-2_value'];
  
  
  $kosyak=false;
  foreach ($errors as $elem)
  	if ($elem==true)
  	$kosyak=true;
  // TODO: аналогично все поля.
  if ($kosyak==false && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД
    // и заполнить переменную $values,
    
    
     $user = 'u23969';
	 $pass = '1986532';
	 $db = new PDO('mysql:host=localhost; dbname=u23969', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

	 $uid=$_SESSION['uid'];
	 $result=$db->query("SELECT name, email, date, pol, konech, bio FROM application2 WHERE id=$uid");
	 
	 // предварительно санитизовав (убираем перед отправкой теги).
	 foreach ($result as $elem ){
		 $values['field-name-1'] = strip_tags($elem['name']);
		 $values['field-email'] =strip_tags($elem['email']);
		 $values['field-date'] =strip_tags($elem['date']);
		 $values['radio-group-1'] =strip_tags($elem['pol']);
		 $values['radio-group-2'] =strip_tags($elem['konech']);
		 $values['field-name-2'] =strip_tags($elem['bio']);
	 }
	 $result=$db->query("SELECT id_s FROM sviaz WHERE id = $uid");
	 $sila=array();
	 foreach ($result as $elem ){
		 $sila[]=(int)strip_tags($elem['id_s']);
		 }
		 $supersil=implode('', $sila);
		 $values['field-name-4']=$supersil;
    printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
  }
  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['field-name-1']) || preg_match('/[^(\x7F-\xFF)|(\s)]/', $_POST['field-name-1'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('field-name-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('field-name-1_value', $_POST['field-name-1'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['field-email'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('field-email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('field-email_value', $_POST['field-email'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['field-date'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('field-date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('field-date_value', $_POST['field-date'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['radio-group-1'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('radio-group-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('radio-group-1_value', $_POST['radio-group-1'], time() + 30 * 24 * 60 * 60);
  }
  
  if (empty($_POST['radio-group-2'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('radio-group-2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('radio-group-2_value', $_POST['radio-group-2'], time() + 30 * 24 * 60 * 60);
  }
  
    if (empty($_POST['field-name-4'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('field-name-4_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
	  $per=implode('',$_POST['field-name-4']);
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('field-name-4_value', $per, time() + 30 * 24 * 60 * 60);
  }
  
      if (empty($_POST['field-name-2'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('field-name-2_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('field-name-2_value', $_POST['field-name-2'], time() + 30 * 24 * 60 * 60);
  }
  
      if (empty($_POST['check-1'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('check-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }


// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('field-name-1_error', '', 100000);
    setcookie('field-email_error', '', 100000);
    setcookie('field-date_error', '', 100000);
    setcookie('radio-group-1_error', '', 100000);
    setcookie('radio-group-2_error', '', 100000);
    setcookie('field-name-4_error', '', 100000);
    setcookie('field-name-2_error', '', 100000);
    setcookie('check-1_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }
    // Сохранение в XML-документ.
  // ...
  $user = 'u23969';
$pass = '1986532';
$db = new PDO('mysql:host=localhost; dbname=u23969', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
	      $uid=$_SESSION['uid'];
	      $stmt = $db->prepare("UPDATE application2 SET name = ?,email=?,date=?,pol=?,konech=?,bio=? WHERE id=$uid");
	      $stmt -> execute([$_POST['field-name-1'],$_POST['field-email'],$_POST['field-date'],$_POST['radio-group-1'],$_POST['radio-group-2'],$_POST['field-name-2']]);
	      $db->query("DELETE FROM sviaz WHERE id=$uid");
	      $stmt = $db->prepare("INSERT INTO sviaz SET id_s=?, id=?");
	      foreach ($_POST['field-name-4'] as $elem)
	      $stmt -> execute([$elem,$uid]);
    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    $login = substr(uniqid(),3,6);
    $pass = substr(md5($_POST['field-name-1']),3,6);
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass', $pass);
    
  
  $stmt = $db->prepare("INSERT INTO application2 SET name = ?,email=?,date=?,pol=?,konech=?,bio=?");
  $stmt -> execute([$_POST['field-name-1'],$_POST['field-email'],$_POST['field-date'],$_POST['radio-group-1'],$_POST['radio-group-2'],$_POST['field-name-2']]);
  $id=$db->lastInsertId();
  $stmt = $db->prepare("INSERT INTO sviaz SET id_s=?, id=?");
  foreach ($_POST['field-name-4'] as $lol) {
  	$stmt -> execute([$lol,$id]);}
  $stmt = $db->prepare("INSERT INTO logipar SET id = ?, login = ?, parol = ?");
  $stmt -> execute([$id,$login,md5($pass)]);
    }
  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
